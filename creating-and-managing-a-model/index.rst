Creating and Managing a Model
=============================



.. toctree::

   the-model-explorer/index
   identifier-declarations/index
   procedures-and-functions/index
   viewing-identifier-selections/index
   debugging-and-profiling-an-aimms-model/index
   the-math-program-inspector/index
