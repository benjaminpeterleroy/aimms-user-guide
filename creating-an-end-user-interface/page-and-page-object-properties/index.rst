.. _chap:prop:

Page and Page Object Properties
===============================



After you have created a page with one or more data objects on it, AIMMS
allows you to modify the display properties of these objects. This
chapter illustrates the available tools for placing and ordering page
objects, and how to modify properties of both pages and page objects. It
also provides a brief description of the available properties.

.. toctree::

   selecting-and-rearranging-page-objects
   modifying-page-and-object-properties
   using-pages-as-dialog-boxes
   defining-user-colors
