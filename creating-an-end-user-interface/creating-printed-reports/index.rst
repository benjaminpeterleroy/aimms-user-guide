.. _chap:print:

Creating Printed Reports
========================



Besides an attractive graphical end-user interface, paper reports
containing the main model results are also an indispensable part of any
successful modeling application. This chapter details printed reports.
Printed reports are created and designed in a similar fashion to
ordinary end-user pages, and can contain the same graphical objects for
displaying data. There is, however, additional support for dividing
large objects over multiple printed pages.

.. toctree::

   print-templates-and-pages
   printing-large-objects-over-multiple-pages
