.. _chap:resize:

Page Resizability
=================

.. rubric:: Resizability

Due to the diversity of objects and their position on a page, it is not
immediately clear how objects should adjust when the size of a page is
changed. Should buttons remain the same, when the size of particular
data objects are changed? Such decisions are up to you, the developer of
the application.



In this chapter, you will learn about the facilities in AIMMS which you
can use to specify how page components should scale when a page size
changes. Such facilities allow you to create *resizable pages* which are
ready for use with different screen resolutions. In addition, resizable
pages let an end-user temporarily enlarge or reduce the size of a
particular page to view more data on the same page, or to simultaneously
look at data on another end-user page.

.. toctree::

   page-resizability
   resizable-templates
   adapting-to-changing-screen-resolutions
