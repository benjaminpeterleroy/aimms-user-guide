.. _chap:pagetool:

Page Management Tools
=====================



When your decision support system grows larger, with possibly several
people developing it, its maintainability aspects become of the utmost
importance. To support you and your co-workers in this task, AIMMS
offers several advanced tools. As discussed in :ref:`chap:model` and
:ref:`chap:view`, the **Model Explorer** combined with the **Identifier
Selector** and **View Manager**, provide you with various useful views
of the model's source code. In this chapter, the specialized AIMMS tools
that will help you set up an advanced end-user interface in an easily
maintainable manner will be introduced.

.. toctree::

   the-page-manager
   the-template-manager
   the-menu-builder
