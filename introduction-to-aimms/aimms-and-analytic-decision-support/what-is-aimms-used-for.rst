What is AIMMS used for?
=======================

.. rubric:: AIMMS usage

AIMMS is used worldwide as a development environment for all kinds of
analytic decision support applications. To give you some insight into
the areas in which AIMMS has been used successfully, this section
describes a small subset of ADS applications, namely those in which
AIMMS itself has been involved (sometimes actively, sometimes at a
distance).

.. rubric:: Crude oil scheduling

The crude oil scheduling system covers the allocation, timetabling,
blending and sequencing activities from the waterfront (arrival of crude
ships) via the crude pipeline to the crude distillation units. The
result is a schedule for the discharge of crudes, the pipeline and the
crude distillers (sequencing, timing and sizing of crude batches), plus
planning indications on the arrival of new crude deliveries. Enhanced
decision support includes improved and timely reaction to changes and
opportunities (e.g. distressed crude cargoes, ship and pumping delays,
operation disturbances) and improved integration between crude
acquisition and unit scheduling.

.. rubric:: Strategic forest management

The strategic forest management system allows foresters to interactively
represent large forested areas at a strategic level. Such a flexible
decision framework can help in understanding how a forest develops over
time. The system also allows one to explore forest management objectives
and their trade-offs, plus the preparation of long-term forest
management plans.

.. rubric:: Transport scheduling in breweries

The transport scheduling system for breweries allows end-users to
interactively steer the flow of products through all phases of the
brewing process from hops to bottled beer. The application can be used
either in an automatic mode where the flow of products is totally
determined by the system, or it can be used in a manual mode where the
user can set or alter the flow using the Gantt chart. The system can
also be used in a simulation mode to test the response of the entire
brewery to varying demands over a longer period of time.

.. rubric:: Risk management

The risk management system for market makers and option traders has a
wide functionality including the theoretical evaluation of derivatives,
an extensive sensitivity analysis, the display of risk profiles, the
generation of scenarios, the generation of price quotes and exercise
signals, minimization of risk exposure, the calculation of exercise
premiums and implied data (volatilities and interest rates), plus an
overview of all transactions for any day.

.. rubric:: Refinery blending

The refinery blending system is a blend scheduling and mixture
optimization system. It is able to handle the complete pooling and
blending problem, and optimizes both the blend schedules and the mixes
under a complete set of (real- life) operational constraints. The system
offers user flexibility in that the user can decide upon the number of
components, fuel mixtures, long versus short term scheduling, and
stand-alone versus refinery-wide scheduling.

.. rubric:: Catalytic cracker support

Catalytic cracking refers to a refining process in which hydrocarbons
are converted into products with a lower molecular mass. The catalytic
cracking support system has three major components: (a) a graphical user
interface consisting of interactive pages, validation routines, plus
reporting and data handling facilities, (b) the model equations,
including those for heat, yields, product properties, economics, and (c)
an on-line process control environment with an off-line mode in which
multiple studies with differing parameters and variables can be
compared.

.. rubric:: Data reconciliation

Data reconciliation is the process of making the smallest possible
adjustment to a collection of measurements within a system such that the
adjusted data values satisfy all the balance constraints applicable to
the system. In the particular application in question, data
reconciliation was applied to a chemical process, requiring that the
relevant mass, component and thermodynamic balances be satisfied for all
units within the system.