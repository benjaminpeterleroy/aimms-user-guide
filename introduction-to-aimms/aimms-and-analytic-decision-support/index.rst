.. _chap:intro:

AIMMS and Analytic Decision Support
===================================

.. rubric:: What is AIMMS?

The acronym AIMMS stands for

   **A**\ dvanced **I**\ ntegrated **M**\ ultidimensional **M**\ odeling
   **S**\ oftware.

AIMMS offers you an easy-to-use and all-round development environment
for creating fully functional *Analytic Decision Support* (ADS)
applications ready for use by end-users. The software is constructed to
run in different modes to support two primary user groups: *modelers*
(application developers) and *end-users* (decision makers). AIMMS
provides the ability to place all of the power of the most advanced
mathematical modeling techniques directly into the hands of the people
who need this to make decisions.



This chapter is aimed at first-time users of the AIMMS modeling system.
In a nutshell, it provides

-  a description of the characteristics of Analytic Decision Support
   (ADS) applications,

-  an overview of AIMMS as an ADS development environment, and

-  some examples of its use in real-life applications.

.. toctree::

   analytic-decision-support
   aimms-as-an-ads-development-environment
   what-is-aimms-used-for
   comparison-with-other-ads-tools
