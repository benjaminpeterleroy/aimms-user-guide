.. _sec:start:demos:

Getting started with AIMMS
==========================

.. rubric:: Learn by example

For most people, learning to use a new software tool like AIMMS is made
substantially easier by first getting to see a few examples of its use.
In this way you can get a quick feel for the AIMMS system, and begin to
form a mental picture of its functionality.

.. rubric:: Getting started quickly

In addition, by taking one or more illustrative examples as a starting
point, you are able to quickly create simple but meaningful AIMMS
projects on your own, without having to read a large amount of
documentation. Building such small projects will further enhance your
understanding of the AIMMS system, both in its use and in its
capabilities.

.. rubric:: AIMMS tutorial

To get you on your way as quickly as possible, the AIMMS system comes
with a tutorial consisting of

-  a number of live demos illustrating the basic use of AIMMS,

-  an extensive library of small example projects each of which
   demonstrates a particular component of either the AIMMS language or
   the end-user interface,

-  a number of complete AIMMS applications, and

-  worked examples corresponding to chapters in the book on optimization
   modeling.

.. rubric:: Example projects

The library of small example projects deals with common tasks such as

-  creating a new project,

-  building a model with your project,

-  data entry,

-  visualizing the results of your model,

-  case management, and

-  various tools, tips and tricks that help you to increase your
   productivity.

.. rubric:: What you learn

By quickly browsing through these examples, you will get a good
understanding of the paradigms underlying the AIMMS technology, and you
will learn the basic steps that are necessary to create a simple, but
fully functional modeling application.

.. rubric:: This `User's Guide <https://documentation.aimms.com/_downloads/AIMMS_user.pdf>`__

Rather than providing an introduction to the use of AIMMS, the User's
Guide deals, in a linear and fairly detailed fashion, with all relevant
aspects of the use of AIMMS and its modeling tools that are necessary to
create a complete modeling application. This information enables you to
use AIMMS to its full potential, and provides answers to questions that
go beyond the scope of the example projects.

.. rubric:: The Language Reference

The Language Reference deals with every aspect of AIMMS data types and
the modeling language. You may need this information to complete the
attribute forms while adding new identifiers to your model, or when you
specify the bodies of procedures and functions in your model.

.. rubric:: The Optimization Modeling guide

The Optimization Modeling guide provides you with the basic principles
of optimization modeling, and also discusses several advanced
optimization techniques which you may find useful when trying to
accomplish nontrivial optimization tasks.

.. rubric:: How to proceed

The following strategy may help you to use AIMMS as efficiently and
quickly as possible.

-  Study some of the working examples to get a good feel for the AIMMS
   system.

-  Select an example project that is close to what you wish to achieve,
   and take it as a starting point for your first modeling project.

-  Consult any of the three AIMMS books whenever you need more thorough
   information about either the use of AIMMS, its language or tips on
   optimization modeling.